from jax import numpy as jnp
from jax import lax

from ..conditionals import base_conditional
from ..covariances import Kuu, Kuf

def gen_q(num_inducing, q_diag=False, num_latent_gps=1):
    q_mu = jnp.zeros((num_inducing, num_latent_gps))

    if q_diag:
        q_sqrt = jnp.ones((num_inducing, num_latent_gps))
    else:
        q_sqrt = jnp.array([
            jnp.eye(num_inducing) for _ in range(num_latent_gps)
        ])

    return q_mu, q_sqrt

def predict_y(
        inducing_variables, q_mu, q_sqrt, Xnew,
        kernel=None, kernel_diag=None, likelihood_mean_var=None,
        whiten=True, jitter=1e-6,
):
    f_mean, f_var = predict_f(
            inducing_variables, q_mu, q_sqrt, Xnew,
            kernel, kernel_diag,
            whiten=whiten, jitter=jitter,
    )
    return likelihood_mean_var(f_mean, f_var)

def elbo(
        inducing_variables, q_mu, q_sqrt,
        X_data, y_data,
        kernel, kernel_diag, lik_var_exp,
        num_data=None, whiten=True, jitter=1e-6,
):
    """
    This gives a variational bound (the evidence lower bound or ELBO) on
    the log marginal likelihood of the model.
    """
    kl = prior_kl(inducing_variables, kernel, q_mu, q_sqrt, whiten=whiten)

    f_mean, f_var = predict_f(
            inducing_variables, q_mu, q_sqrt, X_data,
            kernel, kernel_diag,
            full_cov=False, full_output_cov=False,
            whiten=whiten, jitter=jitter,
    )
    var_exp = lik_var_exp(f_mean, f_var, y_data)

    scale = num_data / X.shape[0] if num_data is not None else 1.0

    return scale * jnp.sum(var_exp) - kl



def predict_f(
        inducing_variables, q_mu, q_sqrt, Xnew,
        kernel, kernel_diag=None,
        full_cov=False, full_output_cov=False,
        whiten=True, jitter=1e-6,
):
    if full_output_cov:
        raise NotImplementedError(
                "full_output_cov (as available in gpflow) isn't implemented"
        )

    full_cov = kernel_diag is None

    Knn = kernel(Xnew) if full_cov else kernel_diag(Xnew)
    Kmm = Kuu(inducing_variables, kernel, jitter=jitter)
    Kmn = Kuf(inducing_variables, kernel, Xnew)

    mu, var = base_conditional(
        Kmn, Kmm, Knn, q_mu,
        q_sqrt=q_sqrt, full_cov=full_cov, white=whiten,
    )
    return mu, var


def prior_kl(inducing_variable, kernel, q_mu, q_sqrt, whiten=False):
    if whiten:
        return gauss_kl(q_mu, q_sqrt, None)
    else:
        K = Kuu(inducing_variable, kernel)  # [P, M, M] or [M, M]
        return gauss_kl(q_mu, q_sqrt, K)


def gauss_kl(q_mu, q_sqrt, K=None):
    """
    Compute the KL divergence KL[q || p] between

          q(x) = N(q_mu, q_sqrt^2)
    and
          p(x) = N(0, K)    if K is not None
          p(x) = N(0, I)    if K is None

    We assume L multiple independent distributions, given by the columns of
    q_mu and the first or last dimension of q_sqrt. Returns the *sum* of the
    divergences.

    q_mu is a matrix ([M, L]), each column contains a mean.

    q_sqrt can be a 3D tensor ([L, M, M]), each matrix within is a lower
        triangular square-root matrix of the covariance of q.
    q_sqrt can be a matrix ([M, L]), each column represents the diagonal of a
        square-root matrix of the covariance of q.

    K is the covariance of p (positive-definite matrix).  The K matrix can be
    passed either directly as `K`, or as its Cholesky factor, `K_cholesky`.  In
    either case, it can be a single matrix [M, M], in which case the sum of the
    L KL divergences is computed by broadcasting, or L different covariances
    [L, M, M].

    Note: if no K matrix is given (both `K` and `K_cholesky` are None),
    `gauss_kl` computes the KL divergence from p(x) = N(0, I) instead.
    """

    is_white = K is None
    is_diag = len(q_sqrt.shape) == 2

    M, L = q_mu.shape

    if is_white:
        alpha = q_mu
    else:
        raise NotImplementedError("non whitened guass_kl() not implemented")

        Lp = lax.linalg.cholesky(K)

        is_batched = len(Lp.shape) == 3

        alpha = lax.linalg.triangular_solve(
                Lm, Kmn,
                left_side=True, lower=True
        )

    if is_diag:
        raise NotImplementedError("diag case of guass_kl() not implemented")
    else:
        Lq = Lq_full = jnp.tril(q_sqrt, 0) # force lower triangle
        Lq_diag = jnp.diagonal(Lq, axis1=-2, axis2=-1)

    # Mahalanobis term: μqᵀ Σp⁻¹ μq
    mahalanobis = jnp.sum(jnp.square(alpha))

    # Constant term: - L * M
    constant = - q_mu.size

    # Log-determinant of the covariance of q(x):
    logdet_qcov = jnp.sum(jnp.log(jnp.square(Lq_diag)))


    # Trace term: tr(Σp⁻¹ Σq)
    if is_white:
        trace = jnp.sum(jnp.square(Lq))
    else:
        raise NotImplementedError("non whitened guass_kl() not implemented")

    twoKL = mahalanobis + constant - logdet_qcov + trace

    return 0.5 * twoKL
