from dataclasses import dataclass, asdict

import jax
from jax import numpy as jnp
from jax.tree_util import Partial

from ...kernel.builders import Matern52Kernel, StationaryKernel
from ...likelihood.builders import GaussianLikelihood

from ..gpr import predict_y, gpr_predict_f, log_marginal_likelihood

@dataclass(frozen=True)
class GPR:
    kernel: StationaryKernel = Matern52Kernel()
    likelihood: GaussianLikelihood = GaussianLikelihood()

    def _check(self):
        if not isinstance(self.kernel, StationaryKernel):
            raise ValueError("GPR's kernel is not a Kernel")
        if not isinstance(self.likelihood, GaussianLikelihood):
            raise ValueError(
                    "GPR's likelihood is not a GaussianLikelihood"
            )

    def build_predict_y(self):
        self._check()
        kernel_func = self.kernel.build()
        kernel_diag_func = self.kernel.build(full_cov=False)
        func = Partial(
                predict_y,
                kernel=kernel_func,
                likelihood=self.likelihood,
                kernel_diag=kernel_diag_func,
        )
        return func

    def build_predict_f(self, full_cov=True):
        self._check()
        kernel_func = self.kernel.build()
        kernel_diag_func = \
                self.kernel.build(full_cov=False) if full_cov else None
        func = Partial(
                gpr_predict_f,
                kernel=kernel_func,
                likelihood_var=self.likelihood.variance,
                kernel_diag=kernel_diag_func,
        )
        return func

    def build_log_marginal_likelihood(self):
        kernel_func = self.kernel.build()
        return Partial(
                log_marginal_likelihood,
                kernel_func=kernel_func,
                likelihood_var=self.likelihood.variance,
        )

    def optimize(self, X_data, y_data, scipy=False, method="BFGS", options=None):

        init_log_params = jnp.log(jnp.array([# log() of initial parameters
            self.kernel.variance,
            self.kernel.lengthscale,
            self.likelihood.variance,
        ]))
        kernel_func = self.kernel.get_kernel_func()

        def loss_closure(log_params):
            params = jnp.exp(log_params)
            kernel = Partial(
                    kernel_func,
                    variance=params[0],
                    lengthscale=params[1],
            )
            return - log_marginal_likelihood(X_data, y_data, kernel, params[2])

        if not scipy:
            from jax.scipy.optimize import minimize

            method = "l-bfgs-experimental-do-not-rely-on-this" \
                    if method == "L-BFGS" else "BFGS"

            @jax.jit
            def minimize_closure():
                return minimize(
                        loss_closure, init_log_params,
                        method=method, options=options,
                )

            result = minimize_closure()

        else:
            if (method == "L-BFGS-B" and
                    jax.config.read("jax_enable_x64") is False):
                raise RuntimeError(
                        "Scipy's L-BFGS-B method doesn't work "
                        "with JAX functions unless float64 are used "
                        "(set jax_enable_x64 True). "
                        "See: https://github.com/google/jax/issues/1510"
                )

            from scipy.optimize import minimize
            import numpy as np

            loss_grad_closure = jax.jit(jax.value_and_grad(loss_closure))

            def loss_grad_closure_np(params):
                loss, grad = loss_grad_closure(params)
                return np.asarray(loss), np.asarray(grad)

            result = minimize(
                    loss_grad_closure_np, init_log_params,
                    jac=True, method=method, options=options,
            )

        opt_params = tuple(jnp.exp(result.x))

        new_kernel = self.kernel \
                .set_variance(opt_params[0]) \
                .set_lengthscale(opt_params[1])

        new_likelihood = GaussianLikelihood(variance=opt_params[2])

        return GPR(new_kernel, new_likelihood), result
