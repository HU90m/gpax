from dataclasses import dataclass

import jax
from jax.tree_util import Partial
from jax import numpy as jnp
import optax

from ...kernel.builders import Matern52Kernel, StationaryKernel
from ...likelihood.builders import GaussianLikelihood
from ...model import svgp
from ...utils import softplus, softplus_inv

@dataclass(frozen=True)
class SVGP:
    kernel: StationaryKernel = Matern52Kernel()
    likelihood: GaussianLikelihood = GaussianLikelihood()
    whiten: bool = True
    jitter: float = 1e-6

    def build_predict_y(self):
        kernel_func = self.kernel.build()
        kernel_diag_func = self.kernel.build(full_cov=False)
        likelihood_mean_var = self.likelihood.build_predict_mean_and_var()
        func = Partial(
                svgp.predict_y,
                kernel=kernel_func,
                kernel_diag=kernel_diag_func,
                likelihood_mean_var=likelihood_mean_var,
                whiten=self.whiten,
        )
        return func


    def build_predict_f(self, full_cov=True):

        kernel_func = self.kernel.build()
        kernel_diag_func = \
                self.kernel.build(full_cov=False) if full_cov else None
        func = Partial(
                svgp.predict_f,
                kernel=kernel_func,
                kernel_diag=kernel_diag_func,
                whiten=self.whiten,
                jitter=self.jitter,
                full_output_cov=full_cov,
        )
        return func

    def build_elbo(self):
        kernel_func = self.kernel.build()
        kernel_diag_func = self.kernel.build(full_cov=False)

        lik_var_exp = self.likelihood.build_variational_expectations()

        func = Partial(
                svgp.elbo,
                kernel=kernel_func,
                kernel_diag=kernel_diag_func,
                lik_var_exp=lik_var_exp,
                whiten=self.whiten,
                jitter=self.jitter,
        )
        return func

    def optimise(
            self, Z, q_mu, q_sqrt, X, Y,
            batch_size=None, num_epochs=1,
            opt_type=optax.adam, opt_kwargs={'learning_rate': 0.01},
            train_inducing_variable=False,
    ):
        X, Y = jnp.array(X), jnp.array(Y)
        Z_static = Z
        init_params = {
            "Z" : Z,
            "q_mu" : q_mu,
            "soft_q_sqrt" : softplus(q_sqrt),
            "log_kern_len" : jnp.log(0.5),
            "log_kern_var" : jnp.log(1.0),
            "log_lik_var" : jnp.log(1.0),
        }
        optimiser = opt_type(**opt_kwargs)
        opt_state = optimiser.init(init_params)
        params = init_params

        if batch_size:
            remainder = X.shape[0] % batch_size
            if remainder:
                print("WARNING: The number of datapoints is not divisible "
                    f"by {batch_size} so the last {remainder} datapoints "
                    "will be ignored.")

                X = X[:-remainder]
                Y = Y[:-remainder]

            num_batches = X.shape[0] // batch_size

            X_new_shape = (num_batches, batch_size) + X.shape[1:]
            Y_new_shape = (num_batches, batch_size) + Y.shape[1:]

            # reshape doesn't allocate new memory
            X_batches = X.reshape(X_new_shape)
            Y_batches = Y.reshape(Y_new_shape)

        else:
            num_batches = 1

            X_batches = jnp.expand_dims(X, 0)
            Y_batches = jnp.expand_dims(Y, 0)

        kernel_mod = self.kernel.get_kernel_mod()
        lik_var_exp = self.likelihood.build_variational_expectations()
        def loss_func(params, X, Y):
            kern_var, kern_len, lik_var = (
                    jnp.exp(params[key])
                    for key in ("log_kern_len", "log_kern_var", "log_lik_var")
            )
            kernel_func = Partial(
                    kernel_mod.kernel, lengthscale=kern_len, variance=kern_var
            )
            kernel_diag_func = Partial(
                    kernel_mod.kernel_diag, variance=lik_var
            )

            Z = params["Z"] if train_inducing_variable else Z_static
            q_mu = params["q_mu"]
            q_sqrt = softplus_inv(params["soft_q_sqrt"])
            return - svgp.elbo(
                Z, q_mu, q_sqrt, X, Y,
                kernel_func, kernel_diag_func, lik_var_exp,
            )

        def opt_step(carry, batch_idx):
            params, opt_state = carry

            #X_batch, Y_batch = data_batches[batch_idx]

            X_batch, Y_batch = X_batches[batch_idx], Y_batches[batch_idx]

            loss, grad = jax.value_and_grad(loss_func)(params, X_batch, Y_batch)

            updates, opt_state = optimiser.update(grad, opt_state)
            params = optax.apply_updates(params, updates)

            return (params, opt_state), loss

        def optimise_epoch(carry, _):
            batch_idxs = jnp.arange(num_batches)
            carry, loss = jax.lax.scan(opt_step, carry, batch_idxs)
            return carry, loss

        @jax.jit
        def optimise_epochs(params, opt_state):
            carry = (params, opt_state)
            return jax.lax.scan(
                    optimise_epoch, carry, None, length=num_epochs,
            )

        (params, opt_state), elbos = optimise_epochs(params, opt_state)

        elbos = jnp.mean(elbos, axis=1)# find average elbos for batches

        new_kernel = self.kernel \
                .set_variance(jnp.exp(params["log_kern_var"])) \
                .set_lengthscale(jnp.exp(params["log_kern_len"]))

        new_likelihood = self.likelihood.set_variance(jnp.exp(params["log_lik_var"]))

        new_svgp = SVGP(
                kernel=new_kernel, likelihood=new_likelihood,
                whiten=self.whiten, jitter=self.jitter
        )

        return (new_svgp, Z, q_mu, q_sqrt), elbos
