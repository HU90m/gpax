from ..conditionals import base_conditional
from ..logdensities import multivariate_normal

from jax import numpy as jnp
from jax import lax


def log_marginal_likelihood(X_data, y_data, kernel_func=None, likelihood_var=1.0):
    K = kernel_func(X_data)
    Ks = add_noise_cov(K, likelihood_var)
    L = lax.linalg.cholesky(Ks)

    log_prob = multivariate_normal(y_data, 0, L)

    return jnp.sum(log_prob)


def predict_y(
        X_data, y_data, Xnew,
        kernel=None,
        kernel_diag=None,
        likelihood=None,
):
    for value, name in (
        (kernel, "kernel"),
        (likelihood, "likelihood"),
        (kernel_diag, "kernel_diag"),
    ):
        if value is None:
            raise ValueError(f"{name} was not provided.")

    lik_var = likelihood.variance
    lik_pred_mean_and_var = likelihood.build_predict_mean_and_var()
    f_mean, f_var = gpr_predict_f(
            X_data, y_data, Xnew, kernel, kernel_diag, lik_var
    )
    return lik_pred_mean_and_var(f_mean, f_var)


def gpr_predict_f(
        X_data, y_data, Xnew,
        kernel=None,
        kernel_diag=None,
        likelihood_var=1.0,
):
    if kernel is None:
        raise ValueError("No kernel provided.")

    err = y_data

    Kmm = kernel(X_data)
    Knn = kernel_diag(Xnew) if kernel_diag else kernel(Xnew)
    Kmn = kernel(X_data, Xnew)
    Kmm_plus_s = add_noise_cov(Kmm, likelihood_var)

    full_cov = False if kernel_diag else True

    f_mean_zero, f_var = base_conditional(
        Kmn, Kmm_plus_s, Knn, err, full_cov=full_cov, white=False
    )  # ([N, P], [N, P]) or ([N, P], [P, N, N])
    f_mean = f_mean_zero
    return f_mean, f_var


def add_noise_cov(K, variance):
    """
    Returns K + σ² I, where σ² is the likelihood noise variance (scalar),
    and I is the corresponding identity matrix.
    """
    return K.at[jnp.diag_indices_from(K)].add(variance)
