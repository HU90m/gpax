from jax import lax
from jax import numpy as jnp

def multivariate_normal(x, mu, L):
    d = x - mu
    alpha = lax.linalg.triangular_solve(L, d, left_side=True, lower=True)
    num_dims = d.shape[0]
    p = -0.5 * jnp.sum(jnp.square(alpha), 0)
    p -= 0.5 * num_dims * jnp.log(2 * jnp.pi)
    p -= jnp.sum(jnp.log(jnp.diag(L)))

    return p
