import jax
import jax.numpy as jnp

def softplus(x):
    #return jnp.log(jnp.exp(x) + 1.)
    return jax.nn.softplus(x)

def softplus_inv(x):
    return jnp.log(jnp.exp(x) - 1.)
