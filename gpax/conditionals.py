import jax.numpy as jnp
from jax import lax

def base_conditional(
    Kmn,
    Kmm,
    Knn,
    f,
    full_cov=False,
    q_sqrt=None,
    white=False,
):
    Lm = lax.linalg.cholesky(Kmm)
    return base_conditional_with_lm(
        Kmn=Kmn, Lm=Lm, Knn=Knn, f=f,
        full_cov=full_cov, q_sqrt=q_sqrt, white=white,
    )

def base_conditional_with_lm(
    Kmn,
    Lm,
    Knn,
    f,
    full_cov=False,
    q_sqrt=None,
    white=False,
):
    """
    # move 0th dimension (M) into the penultimate place.
    # i.e. [M, ..., N] -> [..., M, N]
    ndim = Kmn.ndim
    perm = lax.concatenate(
        [
            lax.reshape(jnp.arange(1, ndim - 1), [ndim - 2]), # other dims
            lax.reshape(0, [1]),  # [M]
            lax.reshape(ndim - 1, [1]), # [N]
        ],
        0,
    )
    Kmn = lax.transpose(Kmn, perm) # [..., M, N]
    """

    # gather dimension sizes
    R = f.shape[-1] # (num_func)
    N = Kmn.shape[-1]
    M = f.shape[-2]
    leading_dims = Kmn.shape[:-2] # (...)


    # compute projection matrix A
    Lm = jnp.broadcast_to(Lm, leading_dims + Lm.shape) # [..., M, M]
    A = lax.linalg.triangular_solve(Lm, Kmn, left_side=True, lower=True) # [..., M, N]

    # compute the conditional covariance
    if full_cov:
        fvar = Knn - A.T @ A # [..., N, N]
        cov_shape = leading_dims + (R, N, N)
        fvar = jnp.broadcast_to(jnp.expand_dims(fvar, -3), cov_shape)  # [..., R, N, N]
    else:
        fvar = Knn - jnp.sum(jnp.square(A), -2)  # [..., N]
        cov_shape = leading_dims + (R, N)
        fvar = jnp.broadcast_to(jnp.expand_dims(fvar, -2), cov_shape)  # [..., R, N]

    # another backsubstitution if unwhitened (I don't know what that means)
    if not white:
        A = lax.linalg.triangular_solve(
                Lm, A,
                left_side=True, lower=True,
                transpose_a=True, conjugate_a=True,
        )

    # compute the conditional mean
    f_shape = leading_dims + (M, R)
    f = jnp.broadcast_to(f, f_shape) # [..., M, R]
    fmean = A.T @ f # [..., N, R]

    if q_sqrt is not None:
        q_sqrt_dims = len(q_sqrt.shape)

        if q_sqrt_dims == 2:
            raise NotImplementedError(
                    f"q_sqrt with dimensions {q_sqrt_dims} "
                    "is not yet implemented"
            )
        elif q_sqrt_dims == 3:
            L = jnp.tril(q_sqrt, 0) # force lower triangle # [R, M, M]
            L = jnp.broadcast_to(L, leading_dims + L.shape)

            shape = leading_dims + (R, M, N)
            A_tiled = jnp.broadcast_to(jnp.expand_dims(A, -3), shape)
            L_T = L.swapaxes(-2, -1) # transpose

            LTA = L_T @ A_tiled
        else:
            raise ValueError(f"Bad dimensions for q_sqrt: {q_sqrt_dims}")

        if full_cov:
            fvar = fvar + LTA.swapaxes(-2, -1) @ LTA # [R, N, N]
        else:
            #print(jnp.sum(jnp.square(LTA), -2))
            fvar = fvar + jnp.sum(jnp.square(LTA), -2) # [R, N]

    if not full_cov:
        fvar = fvar.conj().swapaxes(-2, -1)

    return fmean, fvar
