from jax import numpy as jnp

from .common import stationary_kernel_diag, scaled_squared_euclid_dist

kernel_diag = stationary_kernel_diag

def kernel(X, X2=None, variance=1.0, lengthscale=1.0):
        r2 = scaled_squared_euclid_dist(X, X2, lengthscale)
        return variance * jnp.exp(-0.5 * r2)


