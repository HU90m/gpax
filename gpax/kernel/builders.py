from jax.tree_util import Partial
from dataclasses import dataclass, asdict

from ..kernel import (
        common,
        squared_exponential,
        matern52,
)

@dataclass(frozen=True)
class StationaryKernel:
    variance: float = 1.0
    lengthscale: float = 1.0

    def set_variance(self, variance):
        setup = asdict(self)
        setup['variance'] = variance
        return type(self)(**setup)

    def set_lengthscale(self, lengthscale):
        setup = asdict(self)
        setup['lengthscale'] = lengthscale
        return type(self)(**setup)

    def build(self, jit=False, full_cov=True):
        if not full_cov:
            kernel = Partial(
                    common.stationary_kernel_diag, variance=self.variance,
            )
        else:
            setup = asdict(self)
            kernel_func = self.get_kernel_func()
            kernel = Partial(kernel_func, **setup)

        return kernel

    @staticmethod
    def get_kernel_func():
        return NotImplemented

@dataclass(frozen=True)
class SquaredExponentialKernel(StationaryKernel):
    @staticmethod
    def get_kernel_func():
        return squared_exponential.kernel

@dataclass(frozen=True)
class Matern52Kernel(StationaryKernel):
    @staticmethod
    def get_kernel_func():
        return matern52.kernel

    @staticmethod
    def get_kernel_mod():
        return matern52
