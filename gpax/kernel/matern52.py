from jax import numpy as jnp

from .common import stationary_kernel_diag, scaled_squared_euclid_dist

kernel_diag = stationary_kernel_diag

def kernel(X, X2=None, variance=1.0, lengthscale=1.0):
        r2 = scaled_squared_euclid_dist(X, X2, lengthscale)
        # Clipping around the (single) float precision which is ~1e-45.
        r = jnp.sqrt(jnp.maximum(r2, 1e-36))
        sqrt5 = jnp.sqrt(5.0)
        return variance \
                * (1.0 + sqrt5 * r + 5.0 / 3.0 * jnp.square(r)) \
                * jnp.exp(-sqrt5 * r)
