from jax import numpy as jnp
from jax import lax

def stationary_kernel_diag(X, variance=1.0):
    return jnp.full(X.shape[:-1], variance)


def scaled_squared_euclid_dist(X, X2=None, lengthscale=1.0):
    """
    Returns ‖(X - X2ᵀ) / ℓ‖², i.e. the squared L₂-norm.
    """
    return squared_distance(scale(X, lengthscale), scale(X2, lengthscale))


def scale(X, lengthscale):
    X_scaled = X / lengthscale if X is not None else X
    return X_scaled


def squared_distance(X, X2):
    if X2 is None:
        Xs = jnp.sum(jnp.square(X), axis=-1, keepdims=True)
        dist = -2 * X @ X.T
        dist += Xs + Xs.conj().T
        return dist

    Xs = jnp.sum(jnp.square(X), axis=-1)
    X2s = jnp.sum(jnp.square(X2), axis=-1)
    dist = -2 * jnp.tensordot(X, X2, [[-1], [-1]])
    dist += broadcasting_elementwise(jnp.add, Xs, X2s)
    return dist

def broadcasting_elementwise(op, a, b):
    """
    Apply binary operation `op` to every pair in tensors `a` and `b`.

    :param op: binary operator on tensors, e.g. jnp.add, jnp.substract
    :param a: jnp.ndarray, shape [n_1, ..., n_a]
    :param b: jnp.ndarray, shape [m_1, ..., m_b]
    :return: jnp.ndarray, shape [n_1, ..., n_a, m_1, ..., m_b]
    """
    flatres = op(jnp.reshape(a, [-1, 1]), jnp.reshape(b, [1, -1]))
    return jnp.reshape(flatres, a.shape + b.shape)
