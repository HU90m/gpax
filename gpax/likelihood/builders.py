from dataclasses import dataclass
from jax.tree_util import Partial

from ..likelihood import gaussian

@dataclass(frozen=True)
class Likelihood:
    def build_predict_mean_and_var(self):
        """
        Given a Normal distribution for the latent function,
        return the mean and marginal variance of Y,

        i.e. if
            q(f) = N(Fmu, Fvar)

        and this object represents

            p(y|f)

        then this method computes the predictive mean

           ∫∫ y p(y|f)q(f) df dy

        and the predictive variance

           ∫∫ y² p(y|f)q(f) df dy  - [ ∫∫ y p(y|f)q(f) df dy ]²


        :param Fmu: mean function evaluation Tensor, with shape [..., latent_dim]
        :param Fvar: variance of function evaluation Tensor, with shape [..., latent_dim]
        :returns: mean and variance, both with shape [..., observation_dim]
        """
        return self._build_predict_mean_and_var()

    def build_variational_expectations(self):
        return self._build_variational_expectations()


@dataclass(frozen=True)
class GaussianLikelihood(Likelihood):
    variance: float = 1.0

    def _build_predict_mean_and_var(self):
        return Partial(gaussian.pred_mean_and_var, variance=self.variance)

    def _build_variational_expectations(self):
        return Partial(
                gaussian.variational_expectations,
                likelihood_var=self.variance,
        )

    def set_variance(self, variance):
        return GaussianLikelihood(variance)
