from jax import numpy as jnp

def pred_mean_and_var(Fmu, Fvar, variance=1.0):
        return Fmu, Fvar + variance

def variational_expectations(Fmu, Fvar, Y, likelihood_var=1.0):
    return jnp.sum(
        - 0.5 * jnp.log(2 * jnp.pi)
        - 0.5 * jnp.log(likelihood_var)
        - 0.5 * ((Y - Fmu) ** 2 + Fvar) / likelihood_var,
        axis=-1,
    )
