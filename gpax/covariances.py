from jax import numpy as jnp

def Kuu(inducing_variables, kernel, jitter=0.0):
    num_inducing = inducing_variables.shape[0]
    Kzz = kernel(inducing_variables)
    Kzz += jitter * jnp.eye(num_inducing)
    return Kzz

def Kuf(inducing_variables, kernel, Xnew):
    return kernel(inducing_variables, Xnew)
