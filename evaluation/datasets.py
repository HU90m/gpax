import pandas
import os
from jax import numpy as jnp, random as rand, nn
from urllib.request import urlopen

UCI_BASE_URL = 'https://archive.ics.uci.edu/ml/machine-learning-databases/'

# dictionary of all dataset getters
get_dataset = {}
def add_getter(name):
    def register(function):
        get_dataset.update({name:function})
        return function
    return register

@add_getter('mnist')
def get_mnist(_=None, split=False):
    from tensorflow.keras.datasets import mnist
    # stored in ~/.keras/datasets/mnist.npz
    (X_train, Y_train), (X_test, Y_test) = mnist.load_data(path='mnist.npz')

    #Y_train = jnp.expand_dims(Y_train, 1)
    #Y_test = jnp.expand_dims(Y_test, 1)
    Y_train = nn.one_hot(Y_train, 9)
    Y_test = nn.one_hot(Y_test, 9)

    if split:
        return (X_train, Y_train), (X_test, Y_test)
    else:
        return (X_train, Y_train)

@add_getter('mnist_flat')
def get_mnist_flat(_=None, split=False):
    (X_train, Y_train), (X_test, Y_test) = get_mnist(split=True)

    X_train = X_train.reshape((60000, 28 * 28))
    X_test = X_test.reshape((10000, 28 * 28))

    if split:
        return (X_train, Y_train), (X_test, Y_test)
    else:
        return (X_train, Y_train)

@add_getter('mnist_flat_one')
def get_mnist_flat_one(_=None, split=False):
    (X_train, Y_train), (X_test, Y_test) = get_mnist_flat(split=True)

    Y_test = Y_test[:, :1]
    Y_train = Y_train[:, :1]

    if split:
        return (X_train, Y_train), (X_test, Y_test)
    else:
        return (X_train, Y_train)

@add_getter('boston')
def get_boston(datasets_dir, split=False):
    name = 'boston'
    url = UCI_BASE_URL + 'housing/housing.data'

    dataset_dir = os.path.join(datasets_dir, name)

    if not os.path.isdir(dataset_dir):
        download(url, dataset_dir)

    dataset_file = os.path.join(dataset_dir, url.split('/')[-1])

    data = pandas.read_fwf(dataset_file, header=None).values
    X, Y = data[:, :-1], data[:, -1].reshape(-1, 1)

    if split:
        raise ValueError(
                "This dataset doesn't have pre-seperated "
                "training and testing sets."
        )

    return X, Y

@add_getter('naval')
def get_naval(datasets_dir, split=False):
    name = 'naval'
    url = UCI_BASE_URL + '00316/UCI%20CBM%20Dataset.zip'

    dataset_dir = os.path.join(datasets_dir, name)

    if not os.path.isdir(dataset_dir):
        download(url, dataset_dir)

    dataset_file = os.path.join(dataset_dir, 'UCI CBM Dataset', 'data.txt')

    data = pandas.read_fwf(dataset_file, header=None).values
    X, Y = data[:, :-2], data[:, -2].reshape(-1, 1)

    # dimensions 8 and 11 are constant (std = 0)
    X = jnp.delete(X, jnp.array([8, 11]), axis=1)

    if split:
        raise ValueError(
                "This dataset doesn't have pre-seperated "
                "training and testing sets."
        )

    return X, Y

def shuffle_and_split(key, X, Y, proportion=0.9):
    assert proportion >= 0 and proportion < 1.0

    size_dataset = len(X)
    indicies = jnp.arange(size_dataset)
    shuffled = rand.permutation(key, indicies)# shuffled indicies

    # size of training data
    size_train = int(proportion * size_dataset)

    X_train, Y_train = X[shuffled[:size_train]], Y[shuffled[:size_train]]
    X_test, Y_test = X[shuffled[size_train:]], Y[shuffled[size_train:]]

    return (X_train, Y_train), (X_test, Y_test)

def download(url, dataset_dir):
    print(f"Downloading: {url}")

    os.makedirs(dataset_dir)
    # filename for downloaded file
    dl_filename = os.path.join(dataset_dir, url.split('/')[-1])

    with urlopen(url) as response, open(dl_filename, 'wb') as dl_file:
        data = response.read()
        dl_file.write(data)

    print(f"Downloaded: {url} to {dl_filename}")

    is_zipped = any([url.endswith(ext) for ext in {'.gz', '.zip', '.tar'}])

    if is_zipped:
        print(f"Unzipping: {dl_filename}")
        from zipfile import ZipFile
        with ZipFile(dl_filename, 'r') as zip_file:
            zip_file.extractall(dataset_dir)
