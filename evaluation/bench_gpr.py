#!/bin/python3
import timeit
import argparse
import functools

import jax
import jax.numpy as jnp
import jax.random as rand

import tensorflow as tf
import gpflow

from gpax.model.builders import GPR
from gpax.kernel.builders import SquaredExponentialKernel

import datasets

def gpflow_opt(X, y, scipy=True, method="L-BFGS-B", options={'maxiter': 100}):
    global gpr_opt, result
    kernel_tf = gpflow.kernels.SquaredExponential(lengthscales=0.5)
    gpr = gpflow.models.GPR(data=(X, y), kernel=kernel_tf)

    opt = gpflow.optimizers.Scipy()
    result = opt.minimize(
            gpr.training_loss,
            gpr.trainable_variables,
            options=options,
            method=method,
    )
    gpr_opt = gpr

def gpflow_lik(gpr_opt):
    log_lik = gpr_opt.log_marginal_likelihood()
    return log_lik, tf.exp(log_lik)

def gpax_opt(X, y, scipy=True, method="L-BFGS-B", options={'maxiter': 1}):
    global gpr_opt, result
    kernel = SquaredExponentialKernel(lengthscale=0.5)
    gpr = GPR(kernel=kernel)
    gpr_opt, result = gpr.optimize(
            X, y,
            scipy=scipy, method=method, options=options,
    )

def gpax_lik(gpr_opt, X, Y):
    func = jax.jit(gpr_opt.build_log_marginal_likelihood())
    log_lik = func(X, Y)
    return log_lik, jnp.exp(log_lik)

def parse_arguments():
    parser = argparse.ArgumentParser(
            description="Benchmark the GPR model's quasi-newton optimisation "
                "method in different frameworks."
    )
    parser.add_argument(
            'framework', choices={'gpax', 'gpflow'},
            help="The framework to be benchmarked.",
    )
    parser.add_argument(
            'method', choices={'BFGS', 'L-BFGS-B'},
            help="The optimisation method to be benchmarked.",
    )
    parser.add_argument(
            '--pure-jax', action='store_true', default=False,
            help="When provided, gpax will use JAX's built in "
                "quasi-newton methods as oppose to Scipy's",
    )
    parser.add_argument(
            '-d', '--dataset', type=str, default='boston',
            choices=datasets.get_dataset.keys(),
            help="Dataset to be used in benchmark. "
            "See: https://archive.ics.uci.edu/ml/datasets.php",
    )
    parser.add_argument(
            '--dir', type=str, default='./datasets',
            help="Directory where the datasets are or will be stored",
    )
    parser.add_argument(
            '-n', '--number', type=int, default=1,
            help='Number of timeit runs. The default it 1.',
    )
    parser.add_argument(
            '--dry-run', action='store_true', default=False,
            help='If set, the actual benchmark will not be run.',
    )
    parser.add_argument(
            '--use-f64', action='store_true', default=False,
            help='If set, 64 bit floats will be used as oppose to 32 bit.',
    )
    parser.add_argument(
            '--multivariate', action='store_true', default=False,
            help='If not given, only the first variable in a multivariate '
                'dataset will be use.',
    )
    parser.add_argument(
            '--num-data', type=int, default=None,
            help='Truncate the number of data points to the number given.',
    )
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_arguments()

    default_float = jnp.float64 if args.use_f64 else jnp.float32
    jax.config.update('jax_enable_x64', args.use_f64)
    gpflow.config.set_default_float(default_float)


    X, Y = datasets.get_dataset[args.dataset](args.dir)
    if not args.multivariate: X = X[:, :1]
    if args.num_data:
        X, Y = X[:args.num_data, :], Y[:args.num_data, :]

    X, Y = X.astype(default_float), Y.astype(default_float)
    print(f"Dataset: {args.dataset}, X shape: {X.shape}, Y shape: {Y.shape}")

    if args.pure_jax: assert args.framework != 'gpflow'
    use_scipy = not args.pure_jax

    train_func = gpax_opt if args.framework == 'gpax' else gpflow_opt

    func = functools.partial(
            train_func, X, Y, method=args.method, scipy=use_scipy)

     # These will be filled with the optimised model and optimiser output
    gpr_opt, result = None, None
    if not args.dry_run:
        timer = timeit.Timer(func)
        time_taken = timer.timeit(number=args.number)

    print(f"time taken: {time_taken}")
    print(f"success: {result.success}")

    log_lik, lik = gpax_lik(gpr_opt, X, Y) if args.framework == 'gpax' \
            else gpflow_lik(gpr_opt)

    print(f"log marginal likelihood: {log_lik}")
    print(f"marginal likelihood: {lik}")
