#!/bin/python3
import timeit
import argparse
import functools

import tensorflow as tf
import gpflow

import jax
import jax.numpy as jnp
import jax.random as rand

from gpax import model

import datasets

def main(args):
    default_float = jnp.float64 if args.use_f64 else jnp.float32
    jax.config.update('jax_enable_x64', args.use_f64)
    gpflow.config.set_default_float(default_float)

    X, Y = datasets.get_dataset[args.dataset](args.dir)
    if not args.multivariate: X = X[:, :1]
    if args.num_data:
        X, Y = X[:args.num_data], Y[:args.num_data]
    X, Y = X.astype(default_float), Y.astype(default_float)

    Z = X[:args.num_inducing, :].copy() # inducing points

    print(f"Dataset: {args.dataset}, X shape: {X.shape}, Y shape: {Y.shape}")

    train_func = gpax_opt if args.framework == 'gpax' else gpflow_opt

    func = functools.partial(
            train_func,
            Z, X, Y, num_epochs=args.num_epochs, batch_size=args.batch_size,
            train_inducing_variable=args.train_inducing_variables,
    )

    global elbos; elbos = None # will be filled by func
    timer = timeit.Timer(func, globals=globals())
    time_taken = timer.timeit(number=1)

    print(elbos)
    print(time_taken)


def gpflow_opt_old(
        Z, X, Y,
        num_epochs=1, batch_size=None, learning_rate=0.01,
        train_inducing_variable=False,
    ):
    global elbos

    N = X.shape[0]

    kernel_tf = gpflow.kernels.Matern52(lengthscales=0.5)
    likelihood_tf = gpflow.likelihoods.Gaussian()

    m_tf = gpflow.models.SVGP(kernel_tf, likelihood_tf, Z, num_data=N)

    gpflow.set_trainable(m_tf.inducing_variable, train_inducing_variable)
    #gpflow.set_trainable(m_tf.kernel.variance, False)
    #gpflow.set_trainable(m_tf.kernel.lengthscales, False)
    #gpflow.set_trainable(m_tf.likelihood.variance, False)

    batch_size = batch_size if batch_size else N
    num_batches = int(N / batch_size)
    num_iterations = num_epochs * num_batches

    train_dataset = tf.data.Dataset.from_tensor_slices((X, Y)).repeat().shuffle(N)
    train_iter = iter(train_dataset.batch(batch_size))

    optimizer = tf.optimizers.Adam(learning_rate=learning_rate)
    training_loss = m_tf.training_loss_closure(train_iter)

    @tf.function
    def optimization_step():
        optimizer.minimize(training_loss, m_tf.trainable_variables)

    elbos = []
    for _ in range(num_iterations):
        optimization_step()
        elbos.append( training_loss().numpy() )

def gpflow_opt(
        Z, X, Y,
        num_epochs=1, batch_size=None, learning_rate=0.01,
        train_inducing_variable=False,
    ):
    global elbos

    N = X.shape[0]

    kernel_tf = gpflow.kernels.Matern52(lengthscales=0.5)
    likelihood_tf = gpflow.likelihoods.Gaussian()

    m_tf = gpflow.models.SVGP(kernel_tf, likelihood_tf, Z, num_data=N)

    gpflow.set_trainable(m_tf.inducing_variable, train_inducing_variable)
    #gpflow.set_trainable(m_tf.kernel.variance, False)
    #gpflow.set_trainable(m_tf.kernel.lengthscales, False)
    #gpflow.set_trainable(m_tf.likelihood.variance, False)

    batch_size = batch_size if batch_size else N
    num_batches = N // batch_size
    num_iterations = num_epochs * num_batches

    train_dataset = tf.data.Dataset.from_tensor_slices((X, Y)).repeat().shuffle(N)
    train_iter = iter(train_dataset.batch(batch_size))

    optimizer = tf.optimizers.Adam(learning_rate=learning_rate)
    training_loss = m_tf.training_loss_closure(train_iter)

    @tf.function
    def optimization_step():
        optimizer.minimize(training_loss, m_tf.trainable_variables)

    elbos = []
    for _ in range(num_iterations):
        optimization_step()
        elbos.append( training_loss().numpy() )


def gpax_opt(
        Z, X, Y,
        num_epochs=1, batch_size=None,
        train_inducing_variable=False,
    ):
    global elbos
    M = len(Z)
    q_mu, q_sqrt = model.svgp.gen_q(M)

    svgp_cfg = model.builders.svgp.SVGP()
    _, elbos = svgp_cfg.optimise(
            Z, q_mu, q_sqrt, X, Y,
            batch_size=batch_size, num_epochs=num_epochs,
            train_inducing_variable=False,
    )

def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Benchmark the SVGP model in different frameworks."
    )
    parser.add_argument(
        'framework', choices={'gpax', 'gpflow'},
        help="The framework to be benchmarked.",
    )
    parser.add_argument(
            '-d', '--dataset', type=str, default='boston',
            choices=datasets.get_dataset.keys(),
            help="Dataset to be used in benchmark. "
            "See: https://archive.ics.uci.edu/ml/datasets.php",
    )
    parser.add_argument(
            '--use-f64', action='store_true', default=False,
            help='If set, 64 bit floats will be used as oppose to 32 bit.',
    )
    parser.add_argument(
            '--num-data', type=int, default=None,
            help='Truncate the number of data points to the number given.',
    )
    parser.add_argument(
            '--num-inducing', type=int, default=5,
            help='The number of inducing points to be used.',
    )
    parser.add_argument(
            '--num-epochs', type=int, default=1,
            help='The number of epochs to run.',
    )
    parser.add_argument(
            '--batch-size', type=int, default=None,
            help='The size of the batches to be trained on.',
    )
    parser.add_argument(
            '--multivariate', action='store_true', default=False,
            help='If not given, only the first variable in a multivariate '
                'dataset will be use.',
    )
    parser.add_argument(
            '--train-inducing-variables', action='store_true', default=False,
            help='If not given, only the first variable in a multivariate '
                'dataset will be use.',
    )
    parser.add_argument(
            '--dir', type=str, default='./datasets',
            help="Directory where the datasets are or will be stored",
    )
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()

    main(args)
