#!/bin/python3
import timeit
import argparse
import functools

import tensorflow as tf
import gpflow

import jax
import jax.numpy as jnp
import jax.random as rand

from gpax import model
from gpax.kernel.builders import Matern52Kernel

import datasets

def main(args):
    default_float = jnp.float64 if args.use_f64 else jnp.float32
    jax.config.update('jax_enable_x64', args.use_f64)
    gpflow.config.set_default_float(default_float)

    X, Y = datasets.get_dataset[args.dataset](args.dir)
    if not args.multivariate: X = X[:, :1]
    if args.num_data:
        X, Y = X[:args.num_data], Y[:args.num_data]
    X, Y = X.astype(default_float), Y.astype(default_float)

    Z = X[:args.num_inducing, :].copy() # inducing points

    print(f"Dataset: {args.dataset}, X shape: {X.shape}, Y shape: {Y.shape}")


    elbo_closure = gpax_elbo(Z, X, Y) if args.framework == 'gpax' \
            else gpflow_elbo(Z, X, Y)


    global result; result = None
    timer = timeit.Timer(elbo_closure)
    time_taken = timer.timeit(number=30)

    print(time_taken)
    print(float(result))


def gpflow_elbo(Z, X, Y):

    kernel_tf = gpflow.kernels.Matern52()
    likelihood_tf = gpflow.likelihoods.Gaussian()

    m_tf = gpflow.models.SVGP(kernel_tf, likelihood_tf, Z)

    elbo = tf.function(m_tf.elbo)

    def elbo_closure():
        global result
        result = elbo((X, Y))

    return elbo_closure


def gpax_elbo(Z, X, Y):
    M = len(Z)
    q_mu, q_sqrt = model.svgp.gen_q(M)

    svgp_cfg = model.builders.svgp.SVGP()

    elbo = jax.jit(svgp_cfg.build_elbo())

    def elbo_closure():
        global result
        result = elbo(Z, q_mu, q_sqrt, X, Y)

    return elbo_closure

def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Benchmark the SVGP's elbo method in different frameworks."
    )
    parser.add_argument(
        'framework', choices={'gpax', 'gpflow'},
        help="The framework to be benchmarked.",
    )
    parser.add_argument(
            '-d', '--dataset', type=str, default='boston',
            choices=datasets.get_dataset.keys(),
            help="Dataset to be used in benchmark. "
            "See: https://archive.ics.uci.edu/ml/datasets.php",
    )
    parser.add_argument(
            '--use-f64', action='store_true', default=False,
            help='If set, 64 bit floats will be used as oppose to 32 bit.',
    )
    parser.add_argument(
            '--num-data', type=int, default=None,
            help='Truncate the number of data points to the number given.',
    )
    parser.add_argument(
            '--num-inducing', type=int, default=5,
            help='The number of inducing points to be used.',
    )
    parser.add_argument(
            '--num-runs', type=int, default=1,
            help='The number of runs to average over.',
    )
    parser.add_argument(
            '--multivariate', action='store_true', default=False,
            help='If not given, only the first variable in a multivariate '
                'dataset will be use.',
    )
    parser.add_argument(
            '--dir', type=str, default='./datasets',
            help="Directory where the datasets are or will be stored",
    )
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()

    main(args)
